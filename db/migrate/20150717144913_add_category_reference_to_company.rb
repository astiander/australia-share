class AddCategoryReferenceToCompany < ActiveRecord::Migration
  def change
    add_reference :companies, :category, index: true
    add_foreign_key :companies, :categories
  end
end
