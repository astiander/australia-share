class AddIndexToCompanyBranchId < ActiveRecord::Migration
  def change
    add_index :companies, :company_external_id, unique: true
  end
end
