class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :telephone
      t.string :address
      t.string :company_external_id
      t.string :slug
      t.string :website
      t.integer :postal_code
      t.references :state, index: true
      t.references :city, index: true

      t.timestamps null: false
    end
    add_index :companies, :slug, unique: true
    add_foreign_key :companies, :states
    add_foreign_key :companies, :cities
  end
end
