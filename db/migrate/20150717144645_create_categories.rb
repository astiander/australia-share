class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :slug
      t.references :city, index: true
    end
    add_index :categories, :slug, unique: true
    add_foreign_key :categories, :cities
  end
end
