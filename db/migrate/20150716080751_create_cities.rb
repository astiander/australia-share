class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name
      t.string :slug
      t.references :state, index: true

      t.timestamps null: false
    end
    add_index :cities, :slug, unique: true
    add_foreign_key :cities, :states
  end
end
