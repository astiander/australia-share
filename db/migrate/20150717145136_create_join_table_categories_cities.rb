class CreateJoinTableCategoriesCities < ActiveRecord::Migration
  def change
    create_table :categories_cities, id: false do |t|
      t.belongs_to :category, index: true
      t.belongs_to :city, index: true
    end
  end
end
