class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :abbreviation
      t.string :slug

      t.timestamps null: false
    end
    add_index :states, :slug, unique: true
  end
end
