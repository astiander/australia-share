# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150717145136) do

  create_table "categories", force: :cascade do |t|
    t.string  "name",    limit: 255
    t.string  "slug",    limit: 255
    t.integer "city_id", limit: 4
  end

  add_index "categories", ["city_id"], name: "index_categories_on_city_id", using: :btree
  add_index "categories", ["slug"], name: "index_categories_on_slug", unique: true, using: :btree

  create_table "categories_cities", id: false, force: :cascade do |t|
    t.integer "category_id", limit: 4
    t.integer "city_id",     limit: 4
  end

  add_index "categories_cities", ["category_id"], name: "index_categories_cities_on_category_id", using: :btree
  add_index "categories_cities", ["city_id"], name: "index_categories_cities_on_city_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "slug",       limit: 255
    t.integer  "state_id",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "cities", ["slug"], name: "index_cities_on_slug", unique: true, using: :btree
  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.string   "telephone",           limit: 255
    t.string   "address",             limit: 255
    t.string   "company_external_id", limit: 255
    t.string   "slug",                limit: 255
    t.string   "website",             limit: 255
    t.integer  "postal_code",         limit: 4
    t.integer  "state_id",            limit: 4
    t.integer  "city_id",             limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "category_id",         limit: 4
  end

  add_index "companies", ["category_id"], name: "index_companies_on_category_id", using: :btree
  add_index "companies", ["city_id"], name: "index_companies_on_city_id", using: :btree
  add_index "companies", ["company_external_id"], name: "index_companies_on_company_external_id", unique: true, using: :btree
  add_index "companies", ["slug"], name: "index_companies_on_slug", unique: true, using: :btree
  add_index "companies", ["state_id"], name: "index_companies_on_state_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "states", force: :cascade do |t|
    t.string   "abbreviation", limit: 255
    t.string   "slug",         limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "states", ["slug"], name: "index_states_on_slug", unique: true, using: :btree

  add_foreign_key "categories", "cities"
  add_foreign_key "cities", "states"
  add_foreign_key "companies", "categories"
  add_foreign_key "companies", "cities"
  add_foreign_key "companies", "states"
end
