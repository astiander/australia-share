# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://au.searchalia.com"
SitemapGenerator::Sitemap.sitemaps_path = 'kotl/'
SitemapGenerator::Sitemap.create do
  State.find_each do |state|
    add city_path(state), lastmod: Time.now
  end

  City.find_each do |city|
    add city_categories_path(city), lastmod: Time.now
  end

  Company.find_each do |company|
    add company_path(company), lastmod: Time.now, changefreq: 'monthly'
  end
end
