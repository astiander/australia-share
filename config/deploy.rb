# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'searchalia'
set :repo_url, 'git@bitbucket.org:astiander/australia.git'

set :rvm_ruby_version, '2.2.1'

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/kotl')

after 'deploy:publishing', 'deploy:restart'
namespace :deploy do
  task :restart do
    invoke 'unicorn:restart'
  end
end
