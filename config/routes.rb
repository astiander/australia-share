Rails.application.routes.draw do

    get "/cities/:id", to: "states#show"
    get "/cities/:city_id/categories", to: "cities#show"

  resources :cities , path: "cities" do

    resources :categories , path: "categories"

  end

  post "/company/find", to: "companies#search", as: "companies_search"
  get "/company/find/:query/:query_city", to: "companies#search_results", as: "companies_search_results"

  root "states#index"

  resources :companies, path: "companies"

  resources "contacts", only: [:new, :create]
end
