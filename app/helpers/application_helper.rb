module ApplicationHelper
  def title(page_title, homepage = nil)
    if homepage
      title_string = page_title
    else
      title_string = "#{page_title} - au.Searchalia.com"
    end

    content_for(:title, title_string)
  end

  def keyword_names(page_keyword, homepage = nil)
    if homepage
      keyword_string = page_keyword
    else
      keyword_string = "#{page_keyword}, Australia, search, telephone, address, search address and telephones,business directory, searchalia.com.au"
    end
  end

end
