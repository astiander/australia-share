class City < ActiveRecord::Base
  belongs_to :state
  has_many :companies
  has_and_belongs_to_many :categories


  extend FriendlyId
    friendly_id :name, use: :slugged

  def full_name
    "Company addresses and phone numbers in #{name}, #{state.abbreviation}"
  end

  def keyword_name
    "#{name}, #{state.name}, #{state.abbreviation}"
  end

end
