class Category < ActiveRecord::Base
  belongs_to :state
  has_many :companies
  has_and_belongs_to_many :cities


  extend FriendlyId
    friendly_id :name, use: :slugged

  def keyword_name(city)
    "#{name}, #{city.name}, #{city.state.name}, #{city.state.abbreviation}"
  end

end
