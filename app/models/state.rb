class State < ActiveRecord::Base
  has_many :cities
  has_many :categories


  extend FriendlyId

  friendly_id :slug_formula, use: :slugged
   validates :abbreviation, uniqueness: true
  def slug_formula
    "#{name} #{abbreviation}"
  end

  def keyword_name
    "#{name}, #{abbreviation}"
  end

  def full_name
    "Company addresses and phone numbers in #{name}"
  end

  def name
    map = {
      "AAT" => "Australian Antarctic Territory",
      "ACT" => "Australian Capital Territory",
      "HIMI" => "Heard Island and McDonald Islands",
      "JBT" => "Jervis Bay Territory",
      "NSW" => "New South Wales",
      "NT" => "Northern Territory",
      "QLD" => "Queensland",
      "SA" => "South Australia",
      "TAS" => "Tasmania",
      "VIC" => "Victoria",
      "WA" => "Western Australia"
    }

    map[abbreviation]
  end
end
