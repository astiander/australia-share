class Company < ActiveRecord::Base
  belongs_to :state
  belongs_to :city
  belongs_to :category


  extend FriendlyId
    friendly_id :full_name, use: :slugged
    validates :name, presence: true
    validates :company_external_id, uniqueness: true


  def capitalize_name
    "#{name.mb_chars.capitalize.to_s}"
  end

  def full_name
    "#{capitalize_name} - #{city.name}, #{state.abbreviation}"
  end

  def keyword_name
    "#{capitalize_name}, #{city.name}, #{state.name}, #{state.abbreviation}, #{category.name}"
  end

  def category_company
    near_company = Company.where(category: category, city: city)

    number_of_companies = 10

    number = near_company.count

    empresas = []
    if number >= number_of_companies
      empresas += near_company.includes(:category, :city, :state).order(random).limit(number_of_companies)
    elsif number != 0
      empresas += near_company.includes(:category, :city, :state).order(random).limit(number)
      empresas += Company.where(city: city).includes(:category, :city, :state).order(random).limit(number_of_companies - number)
    else
      empresas += Company.where(city: city).includes(:category, :city, :state).order(random).limit(number)
    end

    empresas
  end

  def self.search(query_param, city_param)
    if query_param.present? && city_param.present?
      city = City.where("name LIKE ?", "#{city_param}%").limit(1).first

      where("name LIKE :query AND city_id = :city_id", {query: "%#{query_param}%", city_id: city.id})
    elsif query_param.present?

    elsif city_param.present?

    else
      nil
    end
  end

private
  def random
    adapter = ActiveRecord::Base.connection.adapter_name #not sure what the real output would be here, test it.
    if adapter == "SQLite"
      "RANDOM()"
    else
      "RAND()"
    end
  end
end
