class CategoriesController < ApplicationController
  def index
    @categories = Category.all
  end

  def show
    @city = City.find_by(slug: params[:city_id])
    @category = Category.find_by(slug: params[:id])
    @companies = Company.where(category: @category, city: @city).order(:slug).page (params[:page])
    @category_full_name = "#{@category.name} company addresses and phone numbers in #{@city.name}, #{@city.state.abbreviation}"
  end


end
