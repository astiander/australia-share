class CitiesController < ApplicationController
 def index
  @cities = City.all
  end

  def show
    @city = City.find_by(slug: params[:city_id])
    @categories = @city.categories.order(:slug).page(params[:page])
  end
end
