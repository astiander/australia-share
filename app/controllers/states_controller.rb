class StatesController < ApplicationController
  def index
    @states = State.all.order(:slug)
  end
  def show
    @state = State.find_by(slug: params[:id])
    @cities = City.where(state: @state).order(:slug).page(params[:page])

    if !@state
      render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found
    end
  end
end


