class CompaniesController < ApplicationController
  def index
    @companies = Company.all
  end

  def show
    @company = Company.friendly.find(params[:id])
  end

  def search
    return redirect_to root_path if params[:query].blank? && params[:query_city].blank?
    redirect_to companies_search_results_path(params[:query], params[:query_city])
  end

  def search_results
    @companies = Company.search(params[:query], params[:query_city]).order(:slug).page(params[:page])
end

end
